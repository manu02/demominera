from django.db import models

# Create your models here.

class MaestroMaquina(models.Model):
    id_equipo = models.CharField(max_length=25, unique=True)
    maquina = models.CharField(max_length=25)
    minera = models.CharField(max_length=25)
    tipo = models.CharField(max_length=25, default=None)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

class calculonorte1(models.Model):
    maquina = models.CharField(max_length=25)
    fecha = models.DateField()
    disponibilidad = models.FloatField()
    utilizacion = models.FloatField()
    stdby_op = models.FloatField(null=True)
    stdby_nop = models.FloatField(null=True)
    horas_op = models.FloatField(null=True)
    stdby_spence = models.FloatField(null=True)
    perforado = models.FloatField(default=None)
    horas_totales = models.FloatField(default=None)
    horas_mantencion = models.FloatField(default=None)
    fallas = models.IntegerField(default=None)
    esp_rep = models.FloatField(default=None)
    class Meta:
        managed = False
        db_table = 'gestionkpi_calculonorte1'

class calculonorte2(models.Model):
    maquina = models.CharField(max_length=25)
    fecha = models.DateField()
    disponibilidad = models.FloatField()
    utilizacion = models.FloatField()
    stdby_op = models.FloatField(null=True)
    stdby_nop = models.FloatField(null=True)
    horas_op = models.FloatField(null=True)
    stdby_cmcc = models.FloatField(null=True)
    perforado = models.FloatField(default=None)
    horas_totales = models.FloatField(default=None)
    horas_mantencion = models.FloatField(default=None)
    fallas = models.IntegerField(default=None)
    esp_rep = models.FloatField(default=None)
    class Meta:
        managed = False
        db_table = 'gestionkpi_calculonorte2'

class norte2(models.Model):
    id_cerro = models.AutoField(primary_key=True)
    holeid = models.CharField(db_column='HOLEID', max_length=45, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=45, blank=True, null=True)  # Field name made lowercase.
    drillingdate = models.DateField(db_column='DRILLINGDATE', blank=True, null=True)  # Field name made lowercase.
    shift = models.CharField(db_column='SHIFT', max_length=25, blank=True, null=True)  # Field name made lowercase.
    mts = models.FloatField(db_column='MTS', blank=True, null=True)  # Field name made lowercase.
    proyecto = models.CharField(db_column='PROYECTO', max_length=45, blank=True, null=True)  # Field name made lowercase.
    prospecto = models.CharField(db_column='PROSPECTO', max_length=45, blank=True, null=True)  # Field name made lowercase.
    total = models.FloatField(db_column='TOTAL', blank=True, null=True)  # Field name made lowercase.
    maquina = models.CharField(db_column='MAQUINA', max_length=45, blank=True, null=True)  # Field name made lowercase.
    rep_equipo = models.FloatField(db_column='REP_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    mant_equipo = models.FloatField(db_column='MANT_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    revisionniveles = models.FloatField(db_column='REVISIONNIVELES', blank=True, null=True)  # Field name made lowercase.
    espera_mecanico = models.FloatField(db_column='ESPERA_MECANICO', blank=True, null=True)  # Field name made lowercase.
    colacion = models.FloatField(db_column='COLACION', blank=True, null=True)  # Field name made lowercase.
    perforacion = models.FloatField(db_column='PERFORACION', blank=True, null=True)  # Field name made lowercase.
    acondicionamiento = models.FloatField(db_column='ACONDICIONAMIENTO', blank=True, null=True)  # Field name made lowercase.
    movimiento_hta = models.FloatField(db_column='MOVIMIENTO_HTA', blank=True, null=True)  # Field name made lowercase.
    tricono = models.FloatField(db_column='Tricono', blank=True, null=True)  # Field name made lowercase.
    inst_equipo = models.FloatField(db_column='INST_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    desarme = models.FloatField(db_column='DESARME', blank=True, null=True)  # Field name made lowercase.
    traslado_equipo = models.FloatField(db_column='TRASLADO_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    medicion = models.FloatField(db_column='MEDICION', blank=True, null=True)  # Field name made lowercase.
    rescate_hta = models.FloatField(db_column='RESCATE_HTA', blank=True, null=True)  # Field name made lowercase.
    instalaciondecasing = models.FloatField(db_column='INSTALACIONDECASING', blank=True, null=True)  # Field name made lowercase.
    movto_barras = models.FloatField(db_column='MOVTO_BARRAS', blank=True, null=True)  # Field name made lowercase.
    rescatecasing = models.FloatField(db_column='RESCATECASING', blank=True, null=True)  # Field name made lowercase.
    prep_lodo = models.FloatField(db_column='PREP_LODO', blank=True, null=True)  # Field name made lowercase.
    preparandosonda = models.FloatField(db_column='PREPARANDOSONDA', blank=True, null=True)  # Field name made lowercase.
    carguiocombustibles = models.FloatField(db_column='CARGUIOCOMBUSTIBLES', blank=True, null=True)  # Field name made lowercase.
    rescateaccesorios = models.FloatField(db_column='RESCATEACCESORIOS', blank=True, null=True)  # Field name made lowercase.
    ensanchepozo = models.FloatField(db_column='ENSANCHEPOZO', blank=True, null=True)  # Field name made lowercase.
    fraguepozo = models.FloatField(db_column='FRAGUEPOZO', blank=True, null=True)  # Field name made lowercase.
    atrapamiento_hta = models.FloatField(db_column='ATRAPAMIENTO_HTA', blank=True, null=True)  # Field name made lowercase.
    empate = models.FloatField(db_column='EMPATE', blank=True, null=True)  # Field name made lowercase.
    instalacionpvc = models.FloatField(db_column='INSTALACIONPVC', blank=True, null=True)  # Field name made lowercase.
    habilitaciondegravas = models.FloatField(db_column='HABILITACIONDEGRAVAS', blank=True, null=True)  # Field name made lowercase.
    lavado_pozo = models.FloatField(db_column='LAVADO_POZO', blank=True, null=True)  # Field name made lowercase.
    desarrollo_pozo = models.FloatField(db_column='DESARROLLO_POZO', blank=True, null=True)  # Field name made lowercase.
    inyeccionlechada = models.FloatField(db_column='INYECCIONLECHADA', blank=True, null=True)  # Field name made lowercase.
    medicionweatherford = models.FloatField(db_column='MEDICIONWEATHERFORD', blank=True, null=True)  # Field name made lowercase.
    secadodemuestra = models.FloatField(db_column='SECADODEMUESTRA', blank=True, null=True)  # Field name made lowercase.
    conf_art = models.FloatField(db_column='CONF_ART', blank=True, null=True)  # Field name made lowercase.
    espera_sup_perf = models.FloatField(db_column='ESPERA_SUP_PERF', blank=True, null=True)  # Field name made lowercase.
    prep_plataforma = models.FloatField(db_column='PREP_PLATAFORMA', blank=True, null=True)  # Field name made lowercase.
    esperamedicion = models.FloatField(db_column='ESPERAMEDICION', blank=True, null=True)  # Field name made lowercase.
    tronadura = models.FloatField(db_column='TRONADURA', blank=True, null=True)  # Field name made lowercase.
    esperaagua = models.FloatField(db_column='ESPERAAGUA', blank=True, null=True)  # Field name made lowercase.
    ingresotronadura = models.FloatField(db_column='INGRESOTRONADURA', blank=True, null=True)  # Field name made lowercase.
    espera_de_plataforma = models.FloatField(db_column='ESPERA_DE_PLATAFORMA', blank=True, null=True)  # Field name made lowercase.
    contpozodecantador = models.FloatField(db_column='CONTPOZODECANTADOR', blank=True, null=True)  # Field name made lowercase.
    espera_cont_perf_cmcc = models.FloatField(db_column='ESPERA_CONT_PERF_CMCC', blank=True, null=True)  # Field name made lowercase.
    despejeportronadura = models.FloatField(db_column='DESPEJEPORTRONADURA', blank=True, null=True)  # Field name made lowercase.
    esperafirmapts = models.FloatField(db_column='ESPERAFIRMAPTS', blank=True, null=True)  # Field name made lowercase.
    charlaapersonal = models.FloatField(db_column='CHARLAAPERSONAL', blank=True, null=True)  # Field name made lowercase.
    carguiodemateriales = models.FloatField(db_column='CARGUIODEMATERIALES', blank=True, null=True)  # Field name made lowercase.
    cambiodeturno = models.FloatField(db_column='CAMBIODETURNO', blank=True, null=True)  # Field name made lowercase.
    esperadechofer = models.FloatField(db_column='ESPERADECHOFER', blank=True, null=True)  # Field name made lowercase.
    espera_eq_apoyo = models.FloatField(db_column='ESPERA_EQ_APOYO', blank=True, null=True)  # Field name made lowercase.
    espera_cont_perf_bly = models.FloatField(db_column='ESPERA_CONT_PERF_BLY', blank=True, null=True)  # Field name made lowercase.
    espera_de_insumo = models.FloatField(db_column='ESPERA_DE_INSUMO', blank=True, null=True)  # Field name made lowercase.
    trasladopersonalaplataforma = models.FloatField(db_column='TRASLADOPERSONALAPLATAFORMA', blank=True, null=True)  # Field name made lowercase.
    esperacombustible = models.FloatField(db_column='ESPERACOMBUSTIBLE', blank=True, null=True)  # Field name made lowercase.
    traslado_personal = models.FloatField(db_column='TRASLADO_PERSONAL', blank=True, null=True)  # Field name made lowercase.
    espera_sup_mina = models.FloatField(db_column='ESPERA_SUP_MINA', blank=True, null=True)  # Field name made lowercase.
    reposohipoclorito = models.FloatField(db_column='ReposoHipoclorito', blank=True, null=True)  # Field name made lowercase.
    inyeccionhipoclorito = models.FloatField(db_column='InyeccionHipoclorito', blank=True, null=True)  # Field name made lowercase.
    preparandoherramienta = models.FloatField(db_column='PreparandoHerramienta', blank=True, null=True)  # Field name made lowercase.
    esperamedicionweatherford = models.FloatField(db_column='EsperaMedicionWeatherford', blank=True, null=True)  # Field name made lowercase.
    varios = models.FloatField(db_column='Varios', blank=True, null=True)  # Field name made lowercase.
    reuniongeotec = models.FloatField(db_column='ReunionGeotec', blank=True, null=True)  # Field name made lowercase.
    habilitacioncabletdr = models.FloatField(db_column='HabilitacionCableTDR', blank=True, null=True)  # Field name made lowercase.
    habilitaciontramie = models.FloatField(db_column='HabilitacionTramie', blank=True, null=True)  # Field name made lowercase.
    preparacionlechada = models.FloatField(db_column='Preparacionlechada', blank=True, null=True)  # Field name made lowercase.
    soldadura = models.FloatField(db_column='Soldadura', blank=True, null=True)  # Field name made lowercase.
    espera_de_ingreso = models.FloatField(db_column='Espera_de_ingreso', blank=True, null=True)  # Field name made lowercase.
    revision_equipo = models.FloatField(db_column='Revision_Equipo', blank=True, null=True)  # Field name made lowercase.
    visita_bhp = models.FloatField(db_column='Visita_BHP', blank=True, null=True)  # Field name made lowercase.
    izaje = models.FloatField(db_column='Izaje', blank=True, null=True)  # Field name made lowercase.
    housekeeping = models.FloatField(db_column='Housekeeping', blank=True, null=True)  # Field name made lowercase.
    medicion_agua = models.FloatField(db_column='Medicion_Agua', blank=True, null=True)  # Field name made lowercase.
    prueba_hidraulica = models.FloatField(db_column='Prueba_Hidraulica', blank=True, null=True)  # Field name made lowercase.
    entrega_muestras = models.FloatField(db_column='Entrega_Muestras', blank=True, null=True)  # Field name made lowercase.
    hrs_mantencion = models.FloatField(db_column='HRS_MANTENCION', blank=True, null=True)  # Field name made lowercase.
    esperarepuestos = models.FloatField(db_column='ESPERAREPUESTOS', blank=True, null=True)  # Field name made lowercase.
    hrs_operacionales = models.FloatField(db_column='HRS_OPERACIONALES', blank=True, null=True)  # Field name made lowercase.
    stby_cmcc = models.FloatField(db_column='STBY_CMCC', blank=True, null=True)  # Field name made lowercase.
    stby_empresaperf = models.FloatField(db_column='STBY_EMPRESAPERF', blank=True, null=True)  # Field name made lowercase.
    disponibilidad = models.FloatField(db_column='DISPONIBILIDAD', blank=True, null=True)  # Field name made lowercase.
    utilizacion = models.FloatField(db_column='UTILIZACION', blank=True, null=True)  # Field name made lowercase.
    mts_stby = models.FloatField(db_column='MTS_STBY', blank=True, null=True)  # Field name made lowercase.
    mts_hr = models.FloatField(db_column='MTS_HR', blank=True, null=True)  # Field name made lowercase.
    mts_hroper = models.FloatField(db_column='MTS_HROPER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'norte2'

class norte1(models.Model):
    id_spence = models.FloatField(primary_key=True)
    dia_turno = models.DateField(db_column='DIA_TURNO', blank=True, null=True)  # Field name made lowercase.
    shift = models.CharField(db_column='SHIFT', max_length=25, blank=True, null=True)  # Field name made lowercase.
    holeid = models.CharField(db_column='HOLEID', max_length=25, blank=True, null=True)  # Field name made lowercase.
    projectcode = models.CharField(db_column='PROJECTCODE', max_length=25, blank=True, null=True)  # Field name made lowercase.
    tipo = models.CharField(db_column='TIPO', max_length=25, blank=True, null=True)  # Field name made lowercase.
    cliente = models.CharField(db_column='CLIENTE', max_length=25, blank=True, null=True)  # Field name made lowercase.
    contractcode = models.CharField(db_column='CONTRACTCODE', max_length=25, blank=True, null=True)  # Field name made lowercase.
    equipo = models.CharField(db_column='EQUIPO', max_length=25, blank=True, null=True)  # Field name made lowercase.
    perforados = models.FloatField(db_column='PERFORADOS', blank=True, null=True)  # Field name made lowercase.
    perforacion = models.FloatField(db_column='PERFORACION', blank=True, null=True)  # Field name made lowercase.
    colacion = models.FloatField(db_column='COLACION', blank=True, null=True)  # Field name made lowercase.
    traslado_colacion = models.FloatField(db_column='TRASLADO_COLACION', blank=True, null=True)  # Field name made lowercase.
    movto_herramientas = models.FloatField(db_column='MOVTO_HERRAMIENTAS', blank=True, null=True)  # Field name made lowercase.
    rescate_barras = models.FloatField(db_column='RESCATE_BARRAS', blank=True, null=True)  # Field name made lowercase.
    traslado_equipo = models.FloatField(db_column='TRASLADO_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    inst_equipo = models.FloatField(db_column='INST_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    rep_equipo = models.FloatField(db_column='REP_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    mant_equipo = models.FloatField(db_column='MANT_EQUIPO', blank=True, null=True)  # Field name made lowercase.
    prep_lodo = models.FloatField(db_column='PREP_LODO', blank=True, null=True)  # Field name made lowercase.
    lavado_pozo = models.FloatField(db_column='LAVADO_POZO', blank=True, null=True)  # Field name made lowercase.
    acond_depozo = models.FloatField(db_column='ACOND_DEPOZO', blank=True, null=True)  # Field name made lowercase.
    mediciondepozo = models.FloatField(db_column='MEDICIONDEPOZO', blank=True, null=True)  # Field name made lowercase.
    conf_art = models.FloatField(db_column='CONF_ART', blank=True, null=True)  # Field name made lowercase.
    esp_camionapoyo = models.FloatField(db_column='ESP_CAMIONAPOYO', blank=True, null=True)  # Field name made lowercase.
    esp_sup_perf = models.FloatField(db_column='ESP_SUP_PERF', blank=True, null=True)  # Field name made lowercase.
    espcliente = models.FloatField(db_column='ESPCLIENTE', blank=True, null=True)  # Field name made lowercase.
    standbymina = models.FloatField(db_column='STANDBYMINA', blank=True, null=True)  # Field name made lowercase.
    standbyperforacion = models.FloatField(db_column='STANDBYPERFORACION', blank=True, null=True)  # Field name made lowercase.
    espera_mecanico = models.FloatField(db_column='ESPERA_MECANICO', blank=True, null=True)  # Field name made lowercase.
    desarrollo_pozo = models.FloatField(db_column='DESARROLLO_POZO', blank=True, null=True)  # Field name made lowercase.
    preparandosonda = models.FloatField(db_column='PREPARANDOSONDA', blank=True, null=True)  # Field name made lowercase.
    reposohipoclorito = models.FloatField(db_column='REPOSOHIPOCLORITO', blank=True, null=True)  # Field name made lowercase.
    rescatecasing = models.FloatField(db_column='RESCATECASING', blank=True, null=True)  # Field name made lowercase.
    prep_plataforma = models.FloatField(db_column='PREP_PLATAFORMA', blank=True, null=True)  # Field name made lowercase.
    tronadura = models.FloatField(db_column='TRONADURA', blank=True, null=True)  # Field name made lowercase.
    fraguepozo = models.FloatField(db_column='FRAGUEPOZO', blank=True, null=True)  # Field name made lowercase.
    esperamedicionwellfield = models.FloatField(db_column='ESPERAMEDICIONWELLFIELD', blank=True, null=True)  # Field name made lowercase.
    rescateaccesorios = models.FloatField(db_column='RESCATEACCESORIOS', blank=True, null=True)  # Field name made lowercase.
    inyeccionhipoclorito = models.FloatField(db_column='INYECCIONHIPOCLORITO', blank=True, null=True)  # Field name made lowercase.
    esperamedicion = models.FloatField(db_column='ESPERAMEDICION', blank=True, null=True)  # Field name made lowercase.
    revisionniveles = models.FloatField(db_column='REVISIONNIVELES', blank=True, null=True)  # Field name made lowercase.
    medicionwellfield = models.FloatField(db_column='MEDICIONWELLFIELD', blank=True, null=True)  # Field name made lowercase.
    ingresotronadura = models.FloatField(db_column='INGRESOTRONADURA', blank=True, null=True)  # Field name made lowercase.
    preparandoherramienta = models.FloatField(db_column='PREPARANDOHERRAMIENTA', blank=True, null=True)  # Field name made lowercase.
    esperamediciongeotec = models.FloatField(db_column='ESPERAMEDICIONGEOTEC', blank=True, null=True)  # Field name made lowercase.
    esperafirmapts = models.FloatField(db_column='ESPERAFIRMAPTS', blank=True, null=True)  # Field name made lowercase.
    inyeccionlechada = models.FloatField(db_column='INYECCIONLECHADA', blank=True, null=True)  # Field name made lowercase.
    varios = models.FloatField(db_column='VARIOS', blank=True, null=True)  # Field name made lowercase.
    reuniongeotec = models.FloatField(db_column='REUNIONGEOTEC', blank=True, null=True)  # Field name made lowercase.
    ensanchepozo = models.FloatField(db_column='ENSANCHEPOZO', blank=True, null=True)  # Field name made lowercase.
    instalaciondecasing = models.FloatField(db_column='INSTALACIONDECASING', blank=True, null=True)  # Field name made lowercase.
    esperacombustible = models.FloatField(db_column='ESPERACOMBUSTIBLE', blank=True, null=True)  # Field name made lowercase.
    contpozodecantador = models.FloatField(db_column='CONTPOZODECANTADOR', blank=True, null=True)  # Field name made lowercase.
    habilitacioncabletdr = models.FloatField(db_column='HABILITACIONCABLETDR', blank=True, null=True)  # Field name made lowercase.
    charlaapersonal = models.FloatField(db_column='CHARLAAPERSONAL', blank=True, null=True)  # Field name made lowercase.
    esperadeagua = models.FloatField(db_column='ESPERADEAGUA', blank=True, null=True)  # Field name made lowercase.
    despejeportronadura = models.FloatField(db_column='DESPEJEPORTRONADURA', blank=True, null=True)  # Field name made lowercase.
    habilitaciondegravas = models.FloatField(db_column='HABILITACIONDEGRAVAS', blank=True, null=True)  # Field name made lowercase.
    instalaciondepvc = models.FloatField(db_column='INSTALACIONDEPVC', blank=True, null=True)  # Field name made lowercase.
    esperadechofer = models.FloatField(db_column='ESPERADECHOFER', blank=True, null=True)  # Field name made lowercase.
    desarme = models.FloatField(db_column='DESARME', blank=True, null=True)  # Field name made lowercase.
    trasladopersonalaplataforma = models.FloatField(db_column='TRASLADOPERSONALAPLATAFORMA', blank=True, null=True)  # Field name made lowercase.
    habilitaciontramie = models.FloatField(db_column='HABILITACIONTRAMIE', blank=True, null=True)  # Field name made lowercase.
    preparacionlechada = models.FloatField(db_column='PREPARACIONLECHADA', blank=True, null=True)  # Field name made lowercase.
    cambiodeturno = models.FloatField(db_column='CAMBIODETURNO', blank=True, null=True)  # Field name made lowercase.
    carguiodecombustibles = models.FloatField(db_column='CARGUIODECOMBUSTIBLES', blank=True, null=True)  # Field name made lowercase.
    carguiodemateriales = models.FloatField(db_column='CARGUIODEMATERIALES', blank=True, null=True)  # Field name made lowercase.
    confecccion_sello_higienico = models.FloatField(db_column='CONFECCCION_SELLO_HIGIENICO', blank=True, null=True)  # Field name made lowercase.
    espera_med_emp_perf = models.FloatField(db_column='ESPERA_MED_EMP_PERF', blank=True, null=True)  # Field name made lowercase.
    espera_med_spence = models.FloatField(db_column='ESPERA_MED_SPENCE', blank=True, null=True)  # Field name made lowercase.
    inst_rev_sist_rotex = models.FloatField(db_column='INST_REV_SIST_ROTEX', blank=True, null=True)  # Field name made lowercase.
    limpieza_piscina = models.FloatField(db_column='LIMPIEZA_PISCINA', blank=True, null=True)  # Field name made lowercase.
    medicion_spence = models.FloatField(db_column='MEDICION_SPENCE', blank=True, null=True)  # Field name made lowercase.
    prep_equi_ensayo = models.FloatField(db_column='PREP_EQUI_ENSAYO', blank=True, null=True)  # Field name made lowercase.
    prueba_leveltroll = models.FloatField(db_column='PRUEBA_LEVELTROLL', blank=True, null=True)  # Field name made lowercase.
    prueba_presiom_bq = models.FloatField(db_column='PRUEBA_PRESIOM_BQ', blank=True, null=True)  # Field name made lowercase.
    pruebas_de_packer = models.FloatField(db_column='PRUEBAS_DE_PACKER', blank=True, null=True)  # Field name made lowercase.
    pruebas_lefranc = models.FloatField(db_column='PRUEBAS_LEFRANC', blank=True, null=True)  # Field name made lowercase.
    pruebas_spt = models.FloatField(db_column='PRUEBAS_SPT', blank=True, null=True)  # Field name made lowercase.
    recuperando_fondo_pozo = models.FloatField(db_column='RECUPERANDO_FONDO_POZO', blank=True, null=True)  # Field name made lowercase.
    reunion_emp_perf = models.FloatField(db_column='REUNION_EMP_PERF', blank=True, null=True)  # Field name made lowercase.
    secado_pozo = models.FloatField(db_column='SECADO_Pozo', blank=True, null=True)  # Field name made lowercase.
    secadodemuestra = models.FloatField(db_column='SECADODEMUESTRA', blank=True, null=True)  # Field name made lowercase.
    espera_condiciones_climaticas = models.FloatField(db_column='ESPERA_CONDICIONES_CLIMATICAS', blank=True, null=True)  # Field name made lowercase.
    total = models.FloatField(db_column='TOTAL', blank=True, null=True)  # Field name made lowercase.
    hrs_mantencion = models.FloatField(db_column='HRS_MANTENCION', blank=True, null=True)  # Field name made lowercase.
    esperarepuestos = models.FloatField(db_column='ESPERAREPUESTOS', blank=True, null=True)  # Field name made lowercase.
    hrs_operacionales = models.FloatField(db_column='HRS_OPERACIONALES', blank=True, null=True)  # Field name made lowercase.
    stnby_spc = models.FloatField(db_column='STNBY_SPC', blank=True, null=True)  # Field name made lowercase.
    stnby_empperf = models.FloatField(db_column='STNBY_EMPPERF', blank=True, null=True)  # Field name made lowercase.
    disponibilidad = models.FloatField(db_column='DISPONIBILIDAD', blank=True, null=True)  # Field name made lowercase.
    utilizacion = models.FloatField(db_column='UTILIZACION', blank=True, null=True)  # Field name made lowercase.
    mts_stby = models.FloatField(db_column='MTS_STBY', blank=True, null=True)  # Field name made lowercase.
    mts_hr = models.FloatField(db_column='MTS_HR', blank=True, null=True)  # Field name made lowercase.
    mts_hroper = models.FloatField(db_column='MTS_HROPER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'norte1'

class Mantenciones(models.Model):
    maquina = models.CharField(max_length=25)
    fecha = models.DateField()
    faena = models.CharField(max_length=25)
    mantenciones_prog = models.IntegerField()
    mantenciones_ejec = models.IntegerField()