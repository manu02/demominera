from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.edit import  CreateView, UpdateView, DeleteView, FormView
from django.views.generic import TemplateView
from .models import MaestroMaquina, calculonorte1, calculonorte2, norte1, norte2
from .forms import UserCreationFormWithEmail
from django.urls import reverse, reverse_lazy
from django.contrib.auth.models import User
from django import forms
from django.core import serializers
from django.db.models import Sum, Q, F
from django.http.response import HttpResponse
from openpyxl import Workbook

import datetime
from dateutil.rrule import rrule, MONTHLY
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
import random
import locale
locale.setlocale(locale.LC_TIME, '')
# Create your views here.
def hex_code_colors():
    a = hex(random.randrange(0,256))
    b = hex(random.randrange(0,256))
    c = hex(random.randrange(0,256))
    a = a[2:]
    b = b[2:]
    c = c[2:]
    if len(a)<2:
        a = "0" + a
    if len(b)<2:
        b = "0" + b
    if len(c)<2:
        c = "0" + c
    z = a + b + c
    return "#" + z.upper()

@login_required
def index(request):
    return render(request, 'index.html')

def comparativo_disp(request):
    return render(request, 'graf_comparativo_disp.html')

def comparativo_util(request):
    return render(request, 'graf_comparativo_util.html')

def comparativo_perf_norte1(request):
    return render(request, 'graf_perf_norte1.html', {'faena':'Norte 1'})

def comparativo_perf_norte2(request):
    return render(request, 'graf_perf_norte2.html', {'faena':'Norte 2'})

def disponibilidad(request):
    template_name = 'graf_disp.html'
    fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})
    
def utilizacion(request):
    template_name = 'graf_util.html'
    fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})

def ege_norte1(request):
    template_name = 'graf_ege_norte1.html'
    fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})

def disponibilidadCMCC(request):
    template_name = 'graf_disp_CMCC.html'
    fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})

def utilizacionCMCC(request):
    template_name = 'graf_util_CMCC.html'
    fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})

def ege_norte2(request):
    template_name = 'graf_ege_norte2.html'
    fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    now = datetime.datetime.now()
    fechainicio = datetime.date(now.year,1,1).strftime("%B-%Y")
    fechafinal = fechas[-1]
    fecha=[]
    fecha.append(fechainicio)
    fecha.append(fechafinal)
    request.session['fecha']=fecha
    del fecha
    if request.method  == 'POST':
        fechainicio=request.POST.get('fecha_inicio')
        fechafinal=request.POST.get('fecha_final')
        fecha=[]
        fecha.append(fechainicio)
        fecha.append(fechafinal)
        request.session['fecha']=fecha
        del fecha
    
    return render(request, template_name, {'fechas':fechas, 'fechast':fechainicio, 'fechaend':fechafinal})

def stdby_CMCC(request):
    template_name = 'graf_stdby.html'
    maquinas = MaestroMaquina.objects.filter(minera="Norte 2").order_by("maquina")
    equipo=MaestroMaquina.objects.filter(minera="Norte 2").values_list('maquina', flat=True).order_by("maquina")
    maquina=equipo[0]
    fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    fecha=fechas_aux.last()
    date=fecha.strftime("%B-%Y")
    request.session['fechastdby'] = fecha.strftime("%Y-%m-01")
    request.session['maquina'] = maquina
    if request.method  == 'POST':
        fechastest=request.POST.get('fechas')
        maquina=request.POST.get('maquina')
        request.session['maquina'] = maquina
        date=fechastest
        fecha=datetime.datetime.strptime(fechastest, "%B-%Y").date()
        fecha=fecha.strftime("%Y-%m-%d")
        request.session['fechastdby'] = fecha
            
    
    return render(request, template_name, {'faena':'Norte 2', 'maquinas':maquinas, 'maquina':maquina, 'fecha':date, 'fechas':fechas})

def stdby_Spence(request):
    template_name = 'graf_stdby_norte1.html'
    maquinas = MaestroMaquina.objects.filter(minera="Norte 1").order_by("maquina")
    equipo=MaestroMaquina.objects.filter(minera="Norte 1").values_list('maquina', flat=True).order_by("maquina")
    maquina=equipo[0]
    fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    fecha=fechas_aux.last()
    date=fecha.strftime("%B-%Y")
    request.session['fechastdby'] = fecha.strftime("%Y-%m-01")
    request.session['maquina'] = maquina
    if request.method  == 'POST':
        fechastest=request.POST.get('fechas')
        maquina=request.POST.get('maquina')
        request.session['maquina'] = maquina
        date=fechastest
        fecha=datetime.datetime.strptime(fechastest, "%B-%Y").date()
        fecha=fecha.strftime("%Y-%m-%d")
        request.session['fechastdby'] = fecha
            
    
    return render(request, template_name, {'faena':'Norte 1', 'maquinas':maquinas, 'maquina':maquina, 'fecha':date, 'fechas':fechas})

def listadoCalculoSpence(request):
    template_name = 'disp.html'
    objetos=calculonorte1.objects.all().order_by('-fecha', 'maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)
    maquinas=MaestroMaquina.objects.all()
    fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    strt_dt_aux='all'
    request.session['filtro']=strt_dt_aux
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    if request.method == 'POST':
        strt_dt_aux=request.POST.get('fechas')
        request.session['filtro']=strt_dt_aux
        if strt_dt_aux == 'all':
            objetos=calculonorte1.objects.all().order_by('-fecha', 'maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)
        else:
            strt_dt=datetime.datetime.strptime(strt_dt_aux, "%B-%Y").date()
            objetos=calculonorte1.objects.filter(fecha=strt_dt).order_by('maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)

        

    return render(request, template_name, {'faena':'Norte 1', 'valor':strt_dt_aux, 'objetos':objetos, 'maquinas':maquinas, 'fechas':fechas})

def listadoCalculoCMCC(request):
    template_name = 'disp.html'
    objetos=calculonorte2.objects.all().order_by('-fecha', 'maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)
    maquinas=MaestroMaquina.objects.all()
    fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
    fechas=[]
    strt_dt_aux='all'
    request.session['filtro']=strt_dt_aux
    for i in fechas_aux:     
        fechas.append(i.strftime("%B-%Y"))
    if request.method == 'POST':
        strt_dt_aux=request.POST.get('fechas')
        request.session['filtro']=strt_dt_aux
        if strt_dt_aux == 'all':
            objetos=calculonorte2.objects.all().order_by('-fecha', 'maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)
        else:
            strt_dt=datetime.datetime.strptime(strt_dt_aux, "%B-%Y").date()
            objetos=calculonorte2.objects.filter(fecha=strt_dt).order_by('maquina').annotate(ege=(F('utilizacion')*F('disponibilidad'))/100)

        

    return render(request, template_name, {'faena':'Norte 2', 'valor':strt_dt_aux, 'objetos':objetos, 'maquinas':maquinas, 'fechas':fechas})

def listadoPerforadosSpence(request):
    template_name = 'perforados.html'
    objetosA = norte1.objects.filter(shift='TURNO-1').values('equipo', 'holeid').annotate(metros=Sum('perforados')).order_by('equipo')
    objetosB = norte1.objects.filter(shift='TURNO-2').values('equipo', 'holeid').annotate(metros=Sum('perforados')).order_by('equipo')
    maquinasddh = MaestroMaquina.objects.filter(minera='Norte 1', tipo='A').values_list('id_equipo', flat=True).distinct()
    maquinasrc = MaestroMaquina.objects.filter(minera='Norte 1', tipo='B').values_list('id_equipo', flat=True).distinct()
    pozos = objetosA.values('holeid', 'equipo') | objetosB.values('holeid', 'equipo')
    objetostotalddh=[]
    objetostotalrc=[]
    subtotalddh=0
    subtotalrc=0
    maquinas = MaestroMaquina.objects.values('id_equipo', 'maquina')
    for i in pozos:
        metros_dia=0.0
        metros_noche=0.0
        equipo=i['equipo']
        holeid=i['holeid']
        if objetosA.filter(holeid=holeid, equipo=equipo):
            metros_dia=round(objetosA.get(holeid=holeid, equipo=equipo)['metros'], 2)
        if objetosB.filter(holeid=holeid, equipo=equipo):
            metros_noche=round(objetosB.get(holeid=holeid, equipo=equipo)['metros'], 2)
        if equipo in maquinasddh:
            if maquinas.filter(id_equipo=equipo):
                equipo=maquinas.get(id_equipo=equipo)['maquina']
            objetostotalddh.append({'equipo':equipo, 'holeid':holeid, 'metros_dia':metros_dia, 'metros_noche':metros_noche, 'metros_total':round(metros_dia+metros_noche, 2)})
            subtotalddh+=metros_dia+metros_noche
        if equipo in maquinasrc:
            if maquinas.filter(id_equipo=equipo):
                equipo=maquinas.get(id_equipo=equipo)['maquina']
            objetostotalrc.append({'equipo':equipo, 'holeid':holeid, 'metros_dia':metros_dia, 'metros_noche':metros_noche, 'metros_total':round(metros_dia+metros_noche, 2)})
            subtotalrc+=metros_dia+metros_noche
            
        
    request.session['mtddh'] = subtotalddh
    request.session['mtrc'] = subtotalrc


    return render(request, template_name, {'faena':'Norte 1', 'objetostotalddh':objetostotalddh, 'objetostotalrc':objetostotalrc})


def listadoPerforadosCMCC(request):
    template_name = 'perforados.html'
    objetosA = norte2.objects.filter(shift='TURNO-1').values('maquina', 'holeid').annotate(metros=Sum('mts')).order_by('maquina')
    objetosB = norte2.objects.filter(shift='TURNO-2').values('maquina', 'holeid').annotate(metros=Sum('mts')).order_by('maquina')
    maquinasddh = MaestroMaquina.objects.filter(minera='Norte 2', tipo='A').values_list('id_equipo', flat=True).distinct()
    maquinasrc = MaestroMaquina.objects.filter(minera='Norte 2', tipo='B').values_list('id_equipo', flat=True).distinct()
    pozos = objetosA.values('holeid', 'maquina') | objetosB.values('holeid', 'maquina')
    objetostotalddh=[]
    objetostotalrc=[]
    subtotalddh=0
    subtotalrc=0
    maquinas = MaestroMaquina.objects.values('id_equipo', 'maquina')
    for i in pozos:
        metros_dia=0.0
        metros_noche=0.0
        equipo=i['maquina']
        holeid=i['holeid']
        if objetosA.filter(holeid=holeid, maquina=equipo):
            metros_dia=round(objetosA.get(holeid=holeid, maquina=equipo)['metros'], 2)
        if objetosB.filter(holeid=holeid, maquina=equipo):
            metros_noche=round(objetosB.get(holeid=holeid, maquina=equipo)['metros'], 2)
        if equipo in maquinasddh:
            if maquinas.filter(id_equipo=equipo):
                equipo=maquinas.get(id_equipo=equipo)['maquina']
            objetostotalddh.append({'equipo':equipo, 'holeid':holeid, 'metros_dia':metros_dia, 'metros_noche':metros_noche, 'metros_total':round(metros_dia+metros_noche, 2)})
            subtotalddh+=metros_dia+metros_noche
        if equipo in maquinasrc:
            if maquinas.filter(id_equipo=equipo):
                equipo=maquinas.get(id_equipo=equipo)['maquina']
            objetostotalrc.append({'equipo':equipo, 'holeid':holeid, 'metros_dia':metros_dia, 'metros_noche':metros_noche, 'metros_total':round(metros_dia+metros_noche, 2)})
            subtotalrc+=metros_dia+metros_noche
            
        
    request.session['mtddh_cmcc'] = subtotalddh
    request.session['mtrc_cmcc'] = subtotalrc


    return render(request, template_name, {'faena':'Norte 2', 'objetostotalddh':objetostotalddh, 'objetostotalrc':objetostotalrc})

def listadoperf_maquina_norte1(request):
    template_name='perf_maquina.html'
    maquina=MaestroMaquina.objects.filter(minera='Norte 1').values('maquina', 'id_equipo', 'tipo').order_by('tipo', 'maquina')
    objetos_aux=calculonorte1.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
    objetos=[]
    subtotalddh = 0
    subtotalrc = 0
    for i in maquina:
        maquina=i['maquina']
        tipo=i['tipo']
        equipo=i['id_equipo']
        if objetos_aux.filter(maquina=equipo):
            metros=round(objetos_aux.get(maquina=equipo)['metros'], 2)
        else:
            metros = 0
            tipo = 'ESTA MÁQUINA NO CONTIENE DATOS PARA LA FAENA SOLICITADA'
        objetos.append({'maquina':maquina, 'tipo':tipo, 'metros':metros})
    for i in objetos:
        tipo = i['tipo']
        metros = i['metros']
        if tipo=='A':
            subtotalddh+=metros
        if tipo=='B':
            subtotalrc+=metros
    request.session['mtddh'] = round(subtotalddh, 2)
    request.session['mtrc'] = round(subtotalrc, 2)

    return render(request, template_name, {'faena':'Norte 1', 'objetos':objetos})

def listadoperf_maquina_norte2(request):
    template_name='perf_maquina.html'
    maquina=MaestroMaquina.objects.filter(minera='Norte 2').values('maquina', 'id_equipo', 'tipo').order_by('tipo', 'maquina')
    objetos_aux=calculonorte2.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
    objetos=[]
    subtotalddh = 0
    subtotalrc = 0
    for i in maquina:
        maquina=i['maquina']
        tipo=i['tipo']
        equipo=i['id_equipo']
        if objetos_aux.filter(maquina=equipo):
            metros=round(objetos_aux.get(maquina=equipo)['metros'], 2)
        else:
            metros = 0
            tipo = 'ESTA MÁQUINA NO CONTIENE DATOS PARA LA FAENA SOLICITADA'
        objetos.append({'maquina':maquina, 'tipo':tipo, 'metros':metros})
    for i in objetos:
        tipo = i['tipo']
        metros = i['metros']
        if tipo=='A':
            subtotalddh+=metros
        if tipo=='B':
            subtotalrc+=metros
    request.session['mtddh_cmcc'] = round(subtotalddh, 2)
    request.session['mtrc_cmcc'] = round(subtotalrc, 2)
    return render(request, template_name, {'faena':'Norte 2', 'objetos':objetos})

def graf_perf_maquina_norte1(request):
    template_name = 'graf_perf_maquina_norte1.html'
    return render(request, template_name, {'faena':'Norte 1'})

def graf_perf_maquina_norte2(request):
    template_name = 'graf_perf_maquina_norte2.html'
    return render(request, template_name, {'faena':'Norte 2'})

class ListarMaquinas(ListView):
    model = MaestroMaquina
    ordering = ['minera', 'id_equipo']

class ListarUsuarios(ListView):
    template_name = 'GestionKPI/user_list.html'
    model = User
    

class CrearMaquina(CreateView):
    model = MaestroMaquina
    fields = ['id_equipo', 'maquina', 'minera', 'tipo']
    success_url = reverse_lazy("listadomaquina")

class CrearUsuario(CreateView):
    form_class = UserCreationFormWithEmail
    template_name = 'GestionKPI/signup.html'
    success_url = reverse_lazy("index") 

    def get_form(self, form_class=None):
        form = super(CrearUsuario, self).get_form()
        # Modificar en tiempo real
        form.fields['username'].widget = forms.TextInput(
            attrs={'class':'form-control mb-2', 'placeholder':'Nombre de usuario'})
        form.fields['email'].widget = forms.EmailInput(
            attrs={'class':'form-control mb-2', 'placeholder':'Dirección email'})
        form.fields['password1'].widget = forms.PasswordInput(
            attrs={'class':'form-control mb-2', 'placeholder':'Contraseña'})
        form.fields['password2'].widget = forms.PasswordInput(
            attrs={'class':'form-control mb-2', 'placeholder':'Repite la contraseña'})
        return form

class ActualizarMaquina(UpdateView):
    model = MaestroMaquina
    fields = ['id_equipo', 'maquina', 'minera', 'tipo']
    template_name_suffix = '_update_form'
    success_url = reverse_lazy("listadomaquina")

class EliminarMaquina(DeleteView):
    model = MaestroMaquina
    success_url = reverse_lazy("listadomaquina")

class EliminarUsuario(DeleteView):
    template_name = 'GestionKPI/user_confirm_delete.html'
    model = User
    success_url = reverse_lazy("listadousuarios")



class ChartDataDispSpence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        maquina = calculonorte1.objects.values_list('maquina', flat=True)
        fecha_nueva = []
        maquina_nueva = []
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        disp = []
        if len(fecha_nueva)==1:
            if calculonorte1.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte1.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                disp = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        disp.append(calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad)
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        disp.append(calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad)
                dataset = [{'data': disp, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:    
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                disp = calculonorte1.objects.filter(maquina = b).values_list('disponibilidad', flat=True)
                fechamaquina = calculonorte1.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in disp :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': z, 'y':z})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del disp
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)



class ChartDataUtilSpence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        maquina = calculonorte1.objects.values_list('maquina', flat=True)
        fecha_nueva = []
        maquina_nueva = []
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        util = []    
        if len(fecha_nueva)==1:
            if calculonorte1.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte1.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                util = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        util.append(calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion)
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        util.append(calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion)
                dataset = [{'data': util, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:    
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                util = calculonorte1.objects.filter(maquina = b).values_list('utilizacion', flat=True)
                fechamaquina = calculonorte1.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in util :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': z, 'y':z})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del util
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)

class ChartDataEGESpence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        maquina = calculonorte1.objects.values_list('maquina', flat=True)
        fecha_nueva = []
        maquina_nueva = []
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        disp = []
        if len(fecha_nueva)==1:
            if calculonorte1.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte1.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                ege = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        disp = calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad
                        util = calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion
                        ege.append(round(disp*util/100, 2))
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        disp = calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad
                        util = calculonorte1.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion
                        ege.append(round(disp*util/100, 2))
                dataset = [{'data': ege, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:     
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                disp = calculonorte1.objects.filter(maquina = b).values('disponibilidad', 'utilizacion')
                fechamaquina = calculonorte1.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in disp :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    ege=round((z['disponibilidad']*z['utilizacion'])/100 ,2)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': ege, 'y':ege})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del disp
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)



class ChartDataDispCMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        maquina = calculonorte2.objects.values_list('maquina', flat=True)
        fecha_nueva = []
        maquina_nueva = []
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        disp = []
        if len(fecha_nueva)==1:
            if calculonorte2.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte2.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                disp = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        disp.append(calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad)
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        disp.append(calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad)
                dataset = [{'data': disp, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:    
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                disp = calculonorte2.objects.filter(maquina = b).values_list('disponibilidad', flat=True)
                fechamaquina = calculonorte2.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in disp :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': z, 'y':z})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del disp
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)


class ChartDataUtilCMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        fecha_nueva = []
        maquina_nueva = []
        maquina = calculonorte2.objects.values_list('maquina', flat=True)
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        util = []    
        if len(fecha_nueva)==1:
            if calculonorte2.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte2.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                util = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        util.append(calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion)
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        util.append(calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion)
                dataset = [{'data': util, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:    
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                util = calculonorte2.objects.filter(maquina = b).values_list('utilizacion', flat=True)
                fechamaquina = calculonorte2.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in util :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': z, 'y':z})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del util
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)

class ChartDataEGECMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fecha']
        fecha_inicio=fecha[0]
        fecha_final=fecha[1]    
        strt_dt = datetime.datetime.strptime(fecha_inicio, "%B-%Y").date()
        end_dt = datetime.datetime.strptime(fecha_final, "%B-%Y").date()
        fecha = [dt for dt in rrule(MONTHLY, dtstart=strt_dt, until=end_dt)]
        maquina = calculonorte2.objects.values_list('maquina', flat=True)
        fecha_nueva = []
        maquina_nueva = []
        for j in maquina:
            if j not in maquina_nueva:
                maquina_nueva.append(j)

        for i in fecha:
            if i not in fecha_nueva:
                fecha_nueva.append(i)
        dataset = []
        disp = []
        if len(fecha_nueva)==1:
            if calculonorte2.objects.filter(fecha=fecha_nueva[0]):
                maquina_nueva = calculonorte2.objects.filter(fecha=fecha_nueva[0]).values_list('maquina', flat=True)
                labels = []
                background = []
                ege = []
                for i in maquina_nueva:
                    if MaestroMaquina.objects.filter(id_equipo=i):
                        labels.append(MaestroMaquina.objects.get(id_equipo=i).maquina)
                        background.append(hex_code_colors())
                        disp = calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad
                        util = calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion
                        ege.append(round(disp*util/100, 2))
                    else:
                        labels.append(i)
                        background.append(hex_code_colors())
                        disp = calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).disponibilidad
                        util = calculonorte2.objects.get(maquina=i, fecha=fecha_nueva[0]).utilizacion
                        ege.append(round(disp*util/100, 2))
                dataset = [{'data': ege, 'backgroundColor':background}]
                data = {
                    "rango":'OFF',
                    "labels": labels,
                    "dataset": dataset
                }
                return Response(data)
        else:  
            for b in maquina_nueva:
                data = []
                auxiliarfecha = []
                disp = calculonorte2.objects.filter(maquina = b).values('disponibilidad', 'utilizacion')
                fechamaquina = calculonorte2.objects.filter(maquina = b).values_list('fecha', flat=True)
                c=0
                for i in fechamaquina:  
                    auxiliarfecha.append(i)
                for z in disp :
                    auxiliar_comp=datetime.datetime.combine(auxiliarfecha[c],datetime.time.min)
                    ege=round((z['disponibilidad']*z['utilizacion'])/100 ,2)
                    if auxiliar_comp  in fecha_nueva:      
                        data.append({'t': auxiliarfecha[c] , 'x': ege, 'y':ege})
                    c+=1
                color=hex_code_colors()
                if data:
                    if MaestroMaquina.objects.filter(id_equipo=b):
                        dataset.append({'label': MaestroMaquina.objects.filter(id_equipo=b).values_list('maquina'), 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                    else:
                        dataset.append({'label': b, 'data': data, 'fill': False, 'backgroundColor': color, 'borderColor': color,})
                del disp
                del data
                del fechamaquina
                del auxiliarfecha
                
            
            data = {
                "rango":'ON',
                "labels":fecha_nueva,
                "dataset": dataset,   
            }
            return Response(data)



class ChartDataComparativo(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        def promediarLista(lista):
            sum=0.0
            for i in lista:
                sum=sum+i
            return round(sum/len(lista),2)
        disponibilidadcmcc = calculonorte2.objects.values_list('disponibilidad', flat=True)
        disponibilidadspence = calculonorte1.objects.values_list('disponibilidad', flat=True)
        promediocmcc=promediarLista(disponibilidadcmcc)
        promediospence=promediarLista(disponibilidadspence)     

        data = {
            "labels":["Norte 1", "Norte 2"],
            "data": [promediospence, promediocmcc],   
        }
        return Response(data)

class ChartDataComparativoUtil(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        def promediarLista(lista):
            sum=0.0
            for i in lista:
                sum=sum+i
            return round(sum/len(lista),2)
        utilcmcc = calculonorte2.objects.values_list('utilizacion', flat=True)
        utilspence = calculonorte1.objects.values_list('utilizacion', flat=True)
        promediocmcc=promediarLista(utilcmcc)
        promediospence=promediarLista(utilspence)     

        data = {
            "labels":["Norte 1", "Norte 2"],
            "data": [promediospence, promediocmcc],   
        }
        return Response(data)


class ChartDataStdby_CMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fechastdby']
        maquina=request.session['maquina']
        equipo=MaestroMaquina.objects.filter(maquina=maquina).values_list('id_equipo', flat=True)
        stdby=calculonorte2.objects.filter(maquina=equipo[0], fecha=fecha).values_list('stdby_op', 'stdby_nop')
        if not stdby:
            stdby=[0, 0]
        data = {
            "labels":["StandBy Operando","StandBy No Operando"],
            "data":stdby[0],
        }
        return Response(data)

class ChartDataStdby_Spence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        fecha=request.session['fechastdby']
        maquina=request.session['maquina']
        equipo=MaestroMaquina.objects.filter(maquina=maquina).values_list('id_equipo', flat=True)
        stdby=calculonorte1.objects.filter(maquina=equipo[0], fecha=fecha).values_list('stdby_op', 'stdby_nop')
        if not stdby:
            stdby=[0, 0]
        

        data = {
            "labels":["StandBy Operando","StandBy No Operando"],
            "data":stdby[0],
        }
        return Response(data)


class PerforSpence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        ddh=request.session['mtddh']
        rc=request.session['mtrc']
        data = {
            "labels":["Metros Perforados A","Metros Perforados B"],
            "data":[ddh, rc],
        }
        return Response(data)

class PerforCMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        ddh=request.session['mtddh_cmcc']
        rc=request.session['mtrc_cmcc']
        data = {
            "labels":["Metros Perforados A","Metros Perforados B"],
            "data":[ddh, rc],
        }
        return Response(data)


class PerforMaquinaSpence(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        objetos = calculonorte1.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
        maquinas = MaestroMaquina.objects.values('id_equipo', 'maquina')
        labels = []
        datos = []
        color = []
        for i in objetos:
            maquina = i['maquina']
            metros = round(i['metros'], 2)
            if maquinas.filter(id_equipo=maquina):
                maquina = maquinas.get(id_equipo=maquina)['maquina']
            labels.append(maquina)
            datos.append(metros)
            color.append(hex_code_colors())

        dataset = [{'data': datos, 'backgroundColor': color}]

        data = {
            "labels":labels,
            "dataset":dataset,
        }
        return Response(data)

class PerforMaquinaCMCC(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, format=None):
        def str_join(*args):
            return ''.join(map(str, args))
        objetos = calculonorte2.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
        maquinas = MaestroMaquina.objects.values('id_equipo', 'maquina')
        labels = []
        datos = []
        color = []
        for i in objetos:
            maquina = i['maquina']
            metros = round(i['metros'], 2)
            if maquinas.filter(id_equipo=maquina):
                maquina = maquinas.get(id_equipo=maquina)['maquina']
            labels.append(maquina)
            datos.append(metros)
            color.append(hex_code_colors())

        dataset = [{'data': datos, 'backgroundColor': color}]

        data = {
            "labels":labels,
            "dataset":dataset,
        }
        return Response(data)

class ReporteSabanaSpence(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        if request.session['filtro']:
            if request.session['filtro']!='all':
                filtro=datetime.datetime.strptime(request.session['filtro'], "%B-%Y").date()
                maquinas= calculonorte1.objects.filter(fecha=filtro)
            else:
                maquinas = calculonorte1.objects.all()

        else:
            maquinas = calculonorte1.objects.all()
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'FECHA'
        ws['C3'] = 'MÁQUINA'
        ws['D3'] = 'DISPONIBILIDAD'
        ws['E3'] = 'UTILIZACIÓN'
        ws['F3'] = 'EGE'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            ws.cell(row=cont,column=2).value = maquina.fecha.strftime("%B-%Y")
            if maquinas_aux.filter(id_equipo=maquina.maquina):
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina.maquina).maquina
            else:
                ws.cell(row=cont,column=3).value = maquina.maquina
            ws.cell(row=cont,column=4).value = maquina.disponibilidad/100
            ws.cell(row=cont,column=4).number_format='0.00%'
            ws.cell(row=cont,column=5).value = maquina.utilizacion/100
            ws.cell(row=cont,column=5).number_format='0.00%'
            ws.cell(row=cont,column=6).value = round((maquina.disponibilidad*maquina.utilizacion)/10000, 4)
            ws.cell(row=cont,column=6).number_format='0.00%'
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReporteSabana_Norte1.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReporteSabanaCMCC(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        if request.session['filtro']:
            if request.session['filtro']!='all':
                filtro=datetime.datetime.strptime(request.session['filtro'], "%B-%Y").date()
                maquinas= calculonorte2.objects.filter(fecha=filtro)
            else:
                maquinas = calculonorte2.objects.all()

        else:
            maquinas = calculonorte2.objects.all()
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'FECHA'
        ws['C3'] = 'MÁQUINA'
        ws['D3'] = 'DISPONIBILIDAD'
        ws['E3'] = 'UTILIZACIÓN'
        ws['F3'] = 'EGE'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            ws.cell(row=cont,column=2).value = maquina.fecha.strftime("%B-%Y")
            if maquinas_aux.filter(id_equipo=maquina.maquina):
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina.maquina).maquina
            else:
                ws.cell(row=cont,column=3).value = maquina.maquina
            ws.cell(row=cont,column=4).value = maquina.disponibilidad/100
            ws.cell(row=cont,column=4).number_format='0.00%'
            ws.cell(row=cont,column=5).value = maquina.utilizacion/100
            ws.cell(row=cont,column=5).number_format='0.00%'
            ws.cell(row=cont,column=6).value = round((maquina.disponibilidad*maquina.utilizacion)/10000, 4)
            ws.cell(row=cont,column=6).number_format='0.00%'
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReporteSabana_Norte 2.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReportePerforacionSpence(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        maquinas = calculonorte1.objects.all()
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'FECHA'
        ws['C3'] = 'MÁQUINA'
        ws['D3'] = 'TIPO'
        ws['E3'] = 'PERFORADO'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            ws.cell(row=cont,column=2).value = maquina.fecha.strftime("%B-%Y")
            if maquinas_aux.filter(id_equipo=maquina.maquina):
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina.maquina).maquina
                ws.cell(row=cont,column=4).value = maquinas_aux.get(id_equipo=maquina.maquina).tipo
            else:
                ws.cell(row=cont,column=3).value = maquina.maquina
                ws.cell(row=cont,column=4).value = '¡Añadir a listado de máquinas!'
            ws.cell(row=cont,column=5).value = maquina.perforado
            
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReportePerforación_Norte1.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReportePerforacionCMCC(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        maquinas = calculonorte2.objects.all()
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA'
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3
        ws['B3'] = 'FECHA'
        ws['C3'] = 'MÁQUINA'
        ws['D3'] = 'TIPO'
        ws['E3'] = 'PERFORADO'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            ws.cell(row=cont,column=2).value = maquina.fecha.strftime("%B-%Y")
            if maquinas_aux.filter(id_equipo=maquina.maquina):
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina.maquina).maquina
                ws.cell(row=cont,column=4).value = maquinas_aux.get(id_equipo=maquina.maquina).tipo
            else:
                ws.cell(row=cont,column=3).value = maquina.maquina
                ws.cell(row=cont,column=4).value = '¡Añadir a listado de máquinas!'
            ws.cell(row=cont,column=5).value = maquina.perforado
            
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReportePerforación_Norte2.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReportePerforacionSpenceAcumulada(TemplateView):
        #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        maquinas = calculonorte1.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
        fechas_aux=calculonorte1.objects.values_list('fecha', flat=True).distinct()
        fechas=[]
        for i in fechas_aux:     
            fechas.append(i.strftime("%B-%Y"))
        fecha=fechas_aux.last()
        date=fecha.strftime("%B-%Y")
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA PERFORACIÓN ACUMULADA HASTA '+ date
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3

        ws['B3'] = 'MÁQUINA'
        ws['C3'] = 'TIPO'
        ws['D3'] = 'PERFORADO'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            if maquinas_aux.filter(id_equipo=maquina['maquina']):
                ws.cell(row=cont,column=2).value = maquinas_aux.get(id_equipo=maquina['maquina']).maquina
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina['maquina']).tipo
            else:
                ws.cell(row=cont,column=2).value = maquina['maquina']
                ws.cell(row=cont,column=3).value = '¡Añadir a listado de máquinas!'
            ws.cell(row=cont,column=4).value = maquina['metros']
            
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReportePerforaciónAcumulada_Norte1.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response

class ReportePerforacionCMCCAcumulada(TemplateView):
        #Usamos el método get para generar el archivo excel 
    def get(self, request, *args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        maquinas = calculonorte2.objects.values('maquina').annotate(metros=Sum('perforado')).order_by('maquina')
        fechas_aux=calculonorte2.objects.values_list('fecha', flat=True).distinct()
        fechas=[]
        for i in fechas_aux:     
            fechas.append(i.strftime("%B-%Y"))
        fecha=fechas_aux.last()
        date=fecha.strftime("%B-%Y")
        maquinas_aux = MaestroMaquina.objects.all()
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['B1'] = 'REPORTE DE SÁBANA PERFORACIÓN ACUMULADA HASTA '+ date
        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('B1:F1')
        #Creamos los encabezados desde la celda B3 hasta la E3

        ws['B3'] = 'MÁQUINA'
        ws['C3'] = 'TIPO'
        ws['D3'] = 'PERFORADO'

        cont=4
        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
        for maquina in maquinas:
            if maquinas_aux.filter(id_equipo=maquina['maquina']):
                ws.cell(row=cont,column=2).value = maquinas_aux.get(id_equipo=maquina['maquina']).maquina
                ws.cell(row=cont,column=3).value = maquinas_aux.get(id_equipo=maquina['maquina']).tipo
            else:
                ws.cell(row=cont,column=2).value = maquina['maquina']
                ws.cell(row=cont,column=3).value = '¡Añadir a listado de máquinas!'
            ws.cell(row=cont,column=4).value = maquina['metros']
            
            cont = cont + 1
        #Establecemos el nombre del archivo
        nombre_archivo ="ReportePerforaciónAcumulada_Norte2.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response