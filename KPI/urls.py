"""KPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from KPI.Apps.GestionKPI import views
from django.contrib.auth.decorators import login_required, permission_required


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls'), name='login'),
    path('', views.index, name='index'),
    path('list_maquina/', login_required(views.ListarMaquinas.as_view()), name='listadomaquina'),
    path('list_usuario/', login_required(views.ListarUsuarios.as_view()), name='listadousuarios'),
    path('create_maquina/', login_required(views.CrearMaquina.as_view()), name='crearMaquina'),
    path('update_maquina/<int:pk>/', login_required(views.ActualizarMaquina.as_view()), name='actualizarMaquina'),
    path('delete_maquina/<int:pk>/', login_required(views.EliminarMaquina.as_view()), name='delete'),
    path('delete_usuario/<int:pk>/', login_required(views.EliminarUsuario.as_view()), name='delete_user'),
    path('chart/norte1/data/disp', login_required(views.ChartDataDispSpence.as_view()), name='chartdatanorte1'),
    path('chart/norte1/data/util', login_required(views.ChartDataUtilSpence.as_view()), name='chartdatautilnorte1'),
    path('chart/CMCC/data/disp', login_required(views.ChartDataDispCMCC.as_view()), name='chartdatanorte2'),
    path('chart/CMCC/data/util', login_required(views.ChartDataUtilCMCC.as_view()), name='chartdatautilnorte2'),
    path('grafico_disp_norte2', login_required(views.disponibilidadCMCC), name='disponibilidad_norte2'),
    path('grafico_util_norte2', login_required(views.utilizacionCMCC), name='util_norte2'),
    path('grafico_util/', login_required(views.utilizacion), name='utilizacion'),
    path('grafico_disp/', login_required(views.disponibilidad), name='disponibilidad'),
    path('create_usuario/', login_required(views.CrearUsuario.as_view()), name='crearUsuario'),
    path('list_disp_norte1/', login_required(views.listadoCalculoSpence), name='listadodisponibilidadSPENCE'),
    path('list_disp_norte2/', login_required(views.listadoCalculoCMCC), name='listadodisponibilidadCMCC'),
    path('chart/data/dispcomp', login_required(views.ChartDataComparativo.as_view()), name='comparativodisponibilidad'),
    path('grafico_comp_disp', login_required(views.comparativo_disp), name='graficocomparativodisp'),
    path('chart/data/utilcomp', login_required(views.ChartDataComparativoUtil.as_view()), name='comparativoutilizacion'),
    path('grafico_comp_util', login_required(views.comparativo_util), name='graficocomparativoutil'),
    path('grafico_stdby_norte2', login_required(views.stdby_CMCC), name='standby_norte2'),
    path('grafico_stdby_norte1', login_required(views.stdby_Spence), name='standby_norte1'),
    path('chart/data/stdby_norte2', login_required(views.ChartDataStdby_CMCC.as_view())),
    path('chart/data/stdby_norte1', login_required(views.ChartDataStdby_Spence.as_view())),
    path('norte1/perforados', login_required(views.listadoPerforadosSpence), name='perfSpence'),
    path('chart/data/perf_norte1', login_required(views.PerforSpence.as_view())),
    path('grafico_perf_norte1', login_required(views.comparativo_perf_norte1), name='grafperfSpence'),
    path('chart/data/ege_norte1', login_required(views.ChartDataEGESpence.as_view())),
    path('grafico_ege_norte1', login_required(views.ege_norte1), name='ege_norte1'),
    path('chart/data/ege_norte2', login_required(views.ChartDataEGECMCC.as_view())),
    path('grafico_ege_norte2', login_required(views.ege_norte2), name='ege_norte2'),
    path('reporte_sabana_norte1', login_required(views.ReporteSabanaSpence.as_view()), name='reporte_norte1'),
    path('reporte_sabana_norte2', login_required(views.ReporteSabanaCMCC.as_view()), name='reporte_norte2'),
    path('norte2/perforados', login_required(views.listadoPerforadosCMCC), name='perfCMCC'),
    path('chart/data/perf_norte2', login_required(views.PerforCMCC.as_view())),
    path('grafico_perf_norte2', login_required(views.comparativo_perf_norte2), name='grafperfCMCC'),
    path('norte1/perforados_maquina', login_required(views.listadoperf_maquina_norte1), name='perf_maquina_Spence'),
    path('norte2/perforados_maquina', login_required(views.listadoperf_maquina_norte2), name='perf_maquina_CMCC'),
    path('chart/data/perforados_maquina', login_required(views.PerforMaquinaSpence.as_view())),
    path('graf_perf_maquina_norte1', login_required(views.graf_perf_maquina_norte1), name='graf_perf_maq_norte1'),
    path('chart/data/perforados_maquina_norte2', login_required(views.PerforMaquinaCMCC.as_view())),
    path('graf_perf_maquina_norte2', login_required(views.graf_perf_maquina_norte2), name='graf_perf_maq_norte2'),
    path('reporte_perforado_norte1', login_required(views.ReportePerforacionSpence.as_view()), name='reporte_perforado_norte1'),
    path('reporte_perforado_norte2', login_required(views.ReportePerforacionCMCC.as_view()), name='reporte_perforado_norte2'),
    path('reporte_perforado_norte1_acumulado', login_required(views.ReportePerforacionSpenceAcumulada.as_view()), name='reporte_perforado_norte1_acumulado'),
    path('reporte_perforado_norte2_acumulado', login_required(views.ReportePerforacionCMCCAcumulada.as_view()), name='reporte_perforado_norte2_acumulado'),


]
